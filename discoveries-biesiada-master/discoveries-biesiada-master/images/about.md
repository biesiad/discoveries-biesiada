# Images

| SOHO-4066             |  SOHO-4053 |
:-------------------------:|:-------------------------:
![SOHO-4066](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4066.jpg)  |  ![SOHO-4053](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4053.jpg)

| SOHO-4050             |  SOHO-4027 |
:-------------------------:|:-------------------------:
![SOHO-4050](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4050.png)  |  ![SOHO-4027](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4027.png)

| SOHO-4026             |  SOHO-4025 |
:-------------------------:|:-------------------------:
![SOHO-4026](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4050.png)  |  ![SOHO-4025](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4025.png)

| SOHO-4022             |  SOHO-4011 |
:-------------------------:|:-------------------------:
![N/A](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/NA.jpg)  |  ![SOHO-4011](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/NA.jpg)

| SOHO-4004             |  SOHO-4002 |
:-------------------------:|:-------------------------:
![SOHO-4004](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4004.jpg)  |  ![SOHO-4002](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-4002.jpg)

| SOHO-3998             |  SOHO-3994 |
:-------------------------:|:-------------------------:
![SOHO-3998](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-3998.jpg)  |  ![SOHO-3994](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-3994.png)

| SOHO-3993             |  SOHO-3991 |
:-------------------------:|:-------------------------:
![SOHO-3993](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-3993.png)  |  ![N/A](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/NA.jpg)

| SOHO-3989             |  SOHO-3277 |
:-------------------------:|:-------------------------:
![SOHO-3989](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-3989.png)  |  ![SOHO-3277](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/NA.jpg)

| SOHO-3277             |  SOHO-3092 |
:-------------------------:|:-------------------------:
![SOHO-2831](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/NA.jpg)  |  ![SOHO-2991](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-3092.jpg)

| SOHO-2991             |  SOHO-2831 |
:-------------------------:|:-------------------------:
![SOHO-2831](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-2991.jpg)  |  ![SOHO-2991](https://gitlab.com/biesiad/discoveries-biesiada/-/raw/master/discoveries-biesiada-master/discoveries-biesiada-master/images/soho-2831.jpg)

Credits: ESA/NASA SOHO/LASCO; Source: [Sungrazer Project](https://sungrazer.nrl.navy.mil/)

Red (C2), blue (C3).
Positions & more details - see [here](https://biesiadamichal.com/), [here](https://soho.nascom.nasa.gov/) or [here](https://sungrazer.nrl.navy.mil/).

Mirrored
